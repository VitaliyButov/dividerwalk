﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoreGame.SO
{
    [CreateAssetMenu(fileName = "Settings", menuName = "ScriptableObjects/LevelSettings)", order = 1)]
    public class LevelSettings : ScriptableObject
    {
        public string[] levelsName;
        //public Sprite[] backgrounds;
        public Material[] wallMaterials;
        public int minNumberLevel;
        public int maxNumberLevel;
    }
}