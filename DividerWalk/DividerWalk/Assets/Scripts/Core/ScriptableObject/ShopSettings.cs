﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoreGame.SO
{
    [CreateAssetMenu(fileName = "ShopSettings", menuName = "ScriptableObjects/ShopSettings", order = 1)]
    public class ShopSettings : ScriptableObject
    {
        public int[] pricePerSkin;
        public int[] pricePerLaser;
    }
}