﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] bool startPlatform;
    public int index;
    PlatformSandwitch platformSandwitch;
    Legs collisionLeg;
    // Start is called before the first frame update
    void Start()
    {
        platformSandwitch = GetComponentInParent<PlatformSandwitch>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
    }

    public void Demolish()
    {
        

        GetComponent<Collider>().enabled = false;
        GetComponentInParent<PlatformSandwitch>().platforms--;
        PlayerScript.Instance.TryFall(collisionLeg,this);
        for(int i = 0; i < transform.childCount; i++)
        {
            Rigidbody rb = transform.GetChild(i).gameObject.AddComponent<Rigidbody>();
            rb.AddForce((transform.GetChild(i).position-transform.position).normalized*10+Vector3.up*2,ForceMode.Impulse);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == Tags.LeftLeg)
        {
            collisionLeg = Legs.Left;
            PlayerScript.Instance.LeftLegCollision(this);
            StartPlatformAction();
        }
        else if(collision.gameObject.tag == Tags.RightLeg)
        {
            collisionLeg = Legs.Right;
            PlayerScript.Instance.RightLegCollision(this);
            StartPlatformAction();
        }
    }
    void StartPlatformAction()
    {
        if (!startPlatform)
        {
            platformSandwitch.platformCollided = true;
            GameManager.Instance.ShowNext(index + 1);
            GetComponentInParent<PlatformDemolisher>().StartDemolisher();
        }
    }
}
