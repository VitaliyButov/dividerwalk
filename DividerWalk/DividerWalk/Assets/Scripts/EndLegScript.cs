﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLegScript : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip extraKnock, regularKnock, knockOrg;
    GameObject player;
    PlayerScript ps;
    GameManager manager;
    
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Divider");
        ps = player.GetComponent<PlayerScript>();
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (!manager.playerFall)
        {
            if (collision.gameObject.tag == "Extra")
            {
                audioSource.clip = extraKnock;
            }
            else if (collision.gameObject.tag == "FinishPlatform" || collision.gameObject.tag == "StartPlatform")
            {
                audioSource.clip = knockOrg;
                audioSource.Play();
                ps.UpdatePositions();
                ps.Collided = true;
                StartCoroutine(setAudioDefault());
                if(collision.gameObject.tag == "FinishPlatform")
                {
                    ps.FinishPlatfromsTouched++;
                }
            }
            else if(collision.gameObject.tag == Tags.Platform)
            {
                audioSource.Play();
                ps.UpdatePositions();
                ps.Collided = true;
                StartCoroutine(setAudioDefault());
                PlayerScript.Instance.UsePhysics(false);
            }
        }
        else
        {
            audioSource.clip = knockOrg;
            audioSource.Play();
            ps.UpdatePositions();
            ps.Collided = true;
            StartCoroutine(setAudioDefault());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        //ps.Collided = false;
    }

    IEnumerator setAudioDefault()
    {
        yield return new WaitUntil(() => !audioSource.isPlaying);
        audioSource.clip = regularKnock;
    }
}
