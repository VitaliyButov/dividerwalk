﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldCoinScript : MonoBehaviour
{
    GameObject mainCoin;
    GameManager manager;
    public GameObject coinDing;
    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        mainCoin = GameObject.FindGameObjectWithTag("MainCoin");
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = (mainCoin.transform.position - gameObject.transform.position);
        if (dir.magnitude < 1f) {
            manager.Score++;
            Destroy(gameObject);
            GameObject coinDingObj = Instantiate(coinDing,gameObject.transform.position,Quaternion.identity);
            Destroy(coinDingObj, 1);
            return;
        }
        dir =dir.normalized;
        gameObject.transform.position = gameObject.transform.position + dir*0.5f;
        gameObject.transform.Rotate(0, 0, 5);
        

    }
}
