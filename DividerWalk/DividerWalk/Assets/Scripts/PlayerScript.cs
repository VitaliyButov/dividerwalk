﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CoreGame.Utils.Template;


public enum Legs
{
    Right,
    Left
}

public class PlayerScript : Singleton<PlayerScript>
{
    [SerializeField] GameObject rightLeg, leftLeg;
    public Vector3 startPos, currPos, movePos,leftLegPivot,rightLegPivot;
    public GameObject LeftLegEnd, RightLegEnd;
    GameObject farestPlatform;
    Platform leftLegPlatform,rightLegPlatform;
    Vector3 currentPivot;
    Rigidbody rb;
    GameManager manager;
    float allDistanceToFinish;
    public float sensivity;
    public Slider slider;
    private bool collided,physicsUsing;
    private int finishPlatfromsTouched;
    public System.DateTime collisionTime;
    public Legs currentleg;
    bool leftLegCollided, rightLegCollided;
    
    public void LeftLegCollision(Platform pl)
    {
        if (leftLegPlatform != pl && rightLegCollided || leftLegPlatform != pl && currentleg == Legs.Right)
        {
            UsePhysics(false);
            UpdatePositions();
            leftLegCollided = true;
            collisionTime = System.DateTime.Now;
            leftLegPlatform = pl;
        }
        else
        {
            Destroy(rightLeg.GetComponent<Rigidbody>());
            Destroy(leftLeg.GetComponent<Rigidbody>());
        }
    }
    public void RightLegCollision(Platform pl)
    {
        if (rightLegPlatform!= pl && leftLegCollided || leftLegPlatform != pl && currentleg == Legs.Left)
        {
            UsePhysics(false);
            UpdatePositions();
            rightLegCollided = true;
            collisionTime = System.DateTime.Now;
            rightLegPlatform = pl;
        }
        else
        {
            Destroy(rightLeg.GetComponent<Rigidbody>());
            Destroy(leftLeg.GetComponent<Rigidbody>());
        }
    }
    public void UsePhysics(bool use)
    {
        if (use)
        {
            physicsUsing = true;
            rb.useGravity = true;
            rb.isKinematic = false;
            rb.constraints = RigidbodyConstraints.None;
        }
        else
        {
            physicsUsing = false;
            rb.useGravity = false;
            rb.isKinematic = true;

            rb.constraints = RigidbodyConstraints.FreezeRotation;
        }
    }

    public bool Collided
    { get
      {
            return collided;
      }
      set
      {
            collided = value;
            try
            {
                slider.value = ((allDistanceToFinish * 1.05f - (farestPlatform.transform.position - gameObject.transform.position).magnitude) / allDistanceToFinish);
            }
            catch (System.NullReferenceException e)
            {

            }
      }
    }

    public int FinishPlatfromsTouched
    {
        get => finishPlatfromsTouched;
        set
        {
            finishPlatfromsTouched = value;
            CheckForWin();
        }
    }

    
    public void TryFall(Legs leg,Platform pl)
    {
            if(leg == Legs.Left)
            {
                if(leftLegPlatform == pl && leftLegCollided)
                {
                    leftLegCollided = false;
                    UsePhysics(true);
                }
            }
            else if (leg == Legs.Right)
            {
                if (rightLegPlatform == pl && rightLegCollided)
                {
                    rightLegCollided = false;
                    UsePhysics(true);
                }
            }
    } 
    void Start()
    {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        currentPivot = leftLegPivot;
        currentleg = Legs.Left;
        rb = GetComponent<Rigidbody>();
        collided = true;
        rightLegCollided = true;
        leftLegCollided = true;
            UpdatePositions();
        Invoke("CalculateDistanceToFinish",1);
    }
    void CalculateDistanceToFinish()
    {
        GameObject[] finishPlatforms = GameObject.FindGameObjectsWithTag("FinishPlatform");
        farestPlatform = finishPlatforms[0];
        for (int i = 0; i < 2; i++)
        {
            if ((finishPlatforms[i].transform.position - gameObject.transform.position).magnitude > (farestPlatform.transform.position - gameObject.transform.position).magnitude)
            {
                farestPlatform = finishPlatforms[i];
                allDistanceToFinish = (farestPlatform.transform.position - gameObject.transform.position).magnitude;
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.position.y < 3)
        {
            manager.playerFall = true;
            Invoke("LoseMenuEnable", 1);
        }
        if (Input.GetMouseButtonDown(0)&&Collided)
        {
            startPos = Input.mousePosition;
            ChangeLeg();
            UpdatePositions();
            StartCoroutine(DividerUp());
            UsePhysics(false);
        }
        if (Input.GetMouseButton(0)&& !physicsUsing)
        {
            currPos = Input.mousePosition;
            movePos = (currPos - startPos);
            transform.RotateAround(currentPivot, Vector3.up, movePos.x * Screen.width * 0.0003f*sensivity);
            startPos = currPos; 
        }
        if (Input.GetMouseButtonUp(0))
        {              
            StartCoroutine(DividerDown());
        }
    }

    void ChangeLeg()
    {
        if (currentleg == Legs.Right)
        {
            currentleg = Legs.Left;
        }
        else 
        { 
            currentleg = Legs.Right;
        }
    }

    IEnumerator DividerUp()
    {
        if (currentleg == Legs.Right)
        {
            rightLegCollided = false;
            for (int i = 0; i < 15; i++)
            {                
                transform.RotateAround(currentPivot, -transform.right, 2f);
                yield return null;
            }
        }        
        else if (currentleg == Legs.Left)
        {
            leftLegCollided = false;
            for (int i = 0; i < 15; i++)
            {
                transform.RotateAround(currentPivot, transform.right, 2f);
                yield return null;
            }
        }
        
    }
    IEnumerator DividerDown()
    {
        if (currentleg == Legs.Right)
        {
            for (int i = 0; i < 15; i++)
            {
                if (!Physics.Raycast(RightLegEnd.transform.position, Vector3.down, 0.2f))
                {
                    if (!rightLegCollided) transform.RotateAround(currentPivot, transform.right, 2f);
                    yield return null;
                }
            }
            if (!rightLegCollided) UsePhysics(true);
        }
        else if (currentleg == Legs.Left)
        {
            for (int i = 0; i < 15; i++)
            {
                if (!Physics.Raycast(LeftLegEnd.transform.position, Vector3.down, 0.2f))
                {
                    if (!leftLegCollided) transform.RotateAround(currentPivot, -transform.right, 2f);
                    yield return null;
                }
            }
            if(!leftLegCollided) UsePhysics(true);
        }
       
    }

    public void UpdatePositions()
    {
        leftLegPivot = LeftLegEnd.transform.position;
        rightLegPivot = RightLegEnd.transform.position;
        if (currentleg == Legs.Left)
        {
            currentPivot = new Vector3(rightLegPivot.x, rightLegPivot.y, rightLegPivot.z);
        }
        else
        {
            currentPivot = new Vector3(leftLegPivot.x, leftLegPivot.y, leftLegPivot.z);
        }
    }

    void LoseMenuEnable()
    {
        manager.loseMenu.SetActive(true);
    }

    void CheckForWin()
    {
        if (finishPlatfromsTouched >= 2)
        {
            manager.Win();
        }
    }

}
