﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinRotator : MonoBehaviour
{
    Camera camera;
    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(0,0,2);
        //gameObject.transform.position = camera.transform.position + new Vector3(-2.7f,-0.33f,11.6f);
        gameObject.transform.position = camera.transform.position + new Vector3(-2.28f,-2.78f,10.2f);
    }
}
