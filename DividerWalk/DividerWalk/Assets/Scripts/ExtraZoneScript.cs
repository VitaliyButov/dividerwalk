﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraZoneScript : MonoBehaviour
{
    CubeScript cs;
    // Start is called before the first frame update
    void Start()
    {
        cs = GetComponentInParent<CubeScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "LegEnd")
        {
            cs.TriggerEnter(other);
        }
    }
}
