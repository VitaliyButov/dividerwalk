﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Tags
{
    public const string Divider = "Divider";
    public const string Platform = "Platform";
    public const string Stick = "Stick";
    public const string LeftLeg = "LeftLeg";
    public const string RightLeg = "RightLeg";
}
