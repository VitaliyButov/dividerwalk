﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CubeScript : MonoBehaviour
{
    bool collided;
    public ParticleSystem main, exhaustSmoke;
    AudioSource audioSource;    
    GameObject moveObject, canvas;
    public GameObject cubeKnock, cubeKnockExtra,textAlmostFall,goldCoin;
    GameManager manager;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        canvas = GameObject.Find("Canvas");
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "LegEnd")
        {
            if (!manager.playerFall)
            {
                if (!collided)
                {
                    GameObject cubeKnockObj = Instantiate(cubeKnock, gameObject.transform);
                    StartCoroutine(cubeKnockScale(cubeKnockObj));
                    manager.PlatformsStepped++;
                }
                collided = true;
                //StartCoroutine(DestroyProcess());
            }

        }
    }
    public void TriggerEnter(Collider other)
    {
        if (!manager.playerFall)
        {
            collided = true;
            GameObject cubeKnockExtraObj = Instantiate(cubeKnockExtra, gameObject.transform);
            StartCoroutine(cubeKnockExtraScale(cubeKnockExtraObj));
            GameObject textAlmostFallObj = Instantiate(textAlmostFall,canvas.transform);
            StartCoroutine(ScaleAndDestroy(textAlmostFallObj));
            manager.DangerZoneStepped++;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        collided = false;        
    }

    IEnumerator DestroyProcess()
    {
        //manager.PlatformsStepped++;
        ParticleSystem ps = GetComponentInChildren<ParticleSystem>();
        Instantiate(goldCoin, gameObject.transform.position, Quaternion.identity);
        for (int i = 0; i < 40; i++)
        {
            gameObject.GetComponent<MeshRenderer>().material.color = new Color(gameObject.GetComponent<MeshRenderer>().material.color.r-0.025f, gameObject.GetComponent<MeshRenderer>().material.color.g-0.025f, gameObject.GetComponent<MeshRenderer>().material.color.b-0.025f);
            ps.startColor = gameObject.GetComponent<MeshRenderer>().material.color;
            yield return new WaitForSeconds(0.06f);
        }
        main.Stop();
        exhaustSmoke.Play();
        audioSource.Play();
        gameObject.GetComponent<BoxCollider>().enabled = false;
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        if (PlayerScript.Instance.Collided) GameObject.FindGameObjectWithTag("Divider").GetComponent<Rigidbody>().useGravity = true;
        Destroy(gameObject, 0.5f);
    }

    public void Fall()
    {
        PlayerScript.Instance.GetComponent<Rigidbody>().useGravity = true;
    }
    IEnumerator cubeKnockScale(GameObject sprite)
    {
        for(int i = 0; i < 40; i++)
        {
            sprite.transform.localScale = new Vector3(sprite.transform.localScale.x + 0.005f, sprite.transform.localScale.y + 0.005f, sprite.transform.localScale.z+0.005f);
            yield return null;
        }
        Destroy(sprite);
    }
    IEnumerator cubeKnockExtraScale(GameObject sprite)
    {
        for (int i = 0; i < 40; i++)
        {
            sprite.transform.localScale = new Vector3(sprite.transform.localScale.x + 0.0075f, sprite.transform.localScale.y + 0.0075f, sprite.transform.localScale.z + 0.0075f);
            yield return null;
        }
        Destroy(sprite);
    }
    IEnumerator ScaleAndDestroy(GameObject text)
    {
        for (int i = 0; i < 30; i++)
        {
            text.transform.localScale = new Vector3(text.transform.localScale.x + 0.3f, text.transform.localScale.y + 0.3f,text.transform.localScale.z+0.3f);
            yield return null;
        }
        for (int i = 0; i < 10; i++)
        {
            TextMeshProUGUI textMesh = text.GetComponent<TextMeshProUGUI>();
            textMesh.color = new Color(textMesh.color.r, textMesh.color.g, textMesh.color.b, textMesh.color.a-0.1f);
            yield return null;
        }
        Destroy(text);
        for (int i = 0; i < 5; i++)
        {
            Instantiate(goldCoin, gameObject.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(0.1f);
        }        
    }
}
