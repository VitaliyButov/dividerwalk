﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDemolisher : MonoBehaviour
{
    bool started;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void StartDemolisher()
    {
        if (!started)
        {
            started = true;
            StartCoroutine(Demolisher());
        }
    }

    IEnumerator Demolisher()
    {
        for (int i = 0; i < 1; i++)
        {
            yield return new WaitForSeconds(1.5f);
        }
        for (int i = 0; i<3; i++)
        {
            transform.GetChild(i).GetComponent<Platform>().Demolish();
            yield return new WaitForSeconds(1.5f);
        }
    }
}
