﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu]
public class LevelSystem : ScriptableObject
{
    private Vector3[] platforms;
    public Vector3 platformOne;
    private int _currentLevelIndex;
    public int CurrentLevelIndex
    {
        get { return _currentLevelIndex; }
        set
        {
            _currentLevelIndex = value;
            SaveData();
        }
    }

    public Vector3[] Platforms { get => platforms; set => platforms = value; }

    public void LoadNextLevel()
    {
        Platforms = new Vector3[0];
        _currentLevelIndex++;
        SaveData();        
        SceneManager.LoadScene(0);
    }


    protected void OnEnable()
    {
        LoadData();
    }


    public void SaveData()
    {
        GameData gd = new GameData();
        gd.currentLevelIndex = _currentLevelIndex;
        BinaryFormatter formatter = new BinaryFormatter();
        BinaryFormatter formatter2 = new BinaryFormatter();
        SurrogateSelector ss = new SurrogateSelector();

        Vector3SerializationSurrogate v3ss = new Vector3SerializationSurrogate();
        ss.AddSurrogate(typeof(Vector3),
                        new StreamingContext(StreamingContextStates.All),
                        v3ss);

        formatter2.SurrogateSelector = ss;
        string path = Application.persistentDataPath + "/GameData.json";
        string path2 = Application.persistentDataPath + "/PlatformsPositions.json";
        FileStream stream = new FileStream(path, FileMode.Create);
        FileStream stream2 = new FileStream(path2, FileMode.Create);

        formatter.Serialize(stream, gd);
        formatter2.Serialize(stream2, platforms);
        stream.Close();
        stream2.Close();
    }
    public void LoadData()
    {
        string path = Application.persistentDataPath + "/GameData.json";
        string path2 = Application.persistentDataPath + "/PlatformsPositions.json";
        if (File.Exists(path) && File.Exists(path2))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            BinaryFormatter formatter2 = new BinaryFormatter();
            SurrogateSelector ss = new SurrogateSelector();
            Vector3SerializationSurrogate v3ss = new Vector3SerializationSurrogate();
            ss.AddSurrogate(typeof(Vector3),
                            new StreamingContext(StreamingContextStates.All),
                            v3ss);

            formatter2.SurrogateSelector = ss;
            FileStream stream = new FileStream(path, FileMode.Open);
            FileStream stream2 = new FileStream(path2, FileMode.Open);
            GameData gd = formatter.Deserialize(stream) as GameData;
            _currentLevelIndex = gd.currentLevelIndex;
            platforms = (Vector3[])formatter2.Deserialize(stream2);
            stream.Close();
            stream2.Close();
            
        }
        else
        {
            Debug.Log("Save file not found");
            _currentLevelIndex = 1;
            Platforms = new Vector3[0];
            platformOne = new Vector3();
            SaveData();
        }
    }
}
[System.Serializable]
public class GameData
{
    public int currentLevelIndex;

}
